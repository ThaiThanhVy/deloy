// B3
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {},
  },
  // Bỏ require('tailwind-scrollbar'), vào đây
  plugins: [require('tailwind-scrollbar'),],
}
