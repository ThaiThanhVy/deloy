import "./App.css";
import { Route, BrowserRouter, Routes } from "react-router-dom";
import HomePage from "./Page/HomePage/HomePage";
import LoginPage from "./Page/LoginPage/LoginPage";
// npm i ant design
import "antd/dist/antd.css";
import DetaiMovies from "./Page/DetaiPage/DetaiMovies";
import Layout from "./Layout/Layout";
import Spinner from "./Components/spinners/Spinner";
import { useSelector } from "react-redux";

function App() {

  return (
    <div>
      <Spinner />
      {/* Spinner */}
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout Component={HomePage} />
            }
          />
          <Route path="/login" element={<LoginPage />} />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetaiMovies} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
