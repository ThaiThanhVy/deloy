import React from 'react'
import Header from '../Components/HeaderThemes/Header'




// Cach 1 
// export default function Layout({ children }) {
//     return (
//         <div>
//             <Header />
//             {children}
//         </div>
//     )
// }

// Cach 2

export default function Layout({ Component }) {
    return (
        <div className='space-y-10'>
            <Header />
            <div><Component /></div>

        </div>
    )
}

