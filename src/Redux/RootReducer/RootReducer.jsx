import { combineReducers } from "redux";
import userReducer from "../Reducers/ReducerUser";
import spinnerReducer from "../Reducers/ReducersSpinner";




export const rootReducer = combineReducers({
    userReducer,
    spinnerReducer,
})