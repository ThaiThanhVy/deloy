
import { localServ } from "../../Services/LocalService"
import { SET_USER } from "../Constants/ConstantUser"


const initialState = {
    userInfor: localServ.user.get(),
}


const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER:
            state.userInfor = action.payload
            return { ...state }
        default: return state
            break;
    }

}

export default userReducer;