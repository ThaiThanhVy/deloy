import { userServ } from "../../Services/UserService"
import { SET_USER } from "../Constants/ConstantUser";

const setUserLoginSuccess = (successValue) => {
    return {
        type: SET_USER,
        payload: successValue,
    }
}

export const setUserLoginActionServ = (dataLogin , onLoginSuccess , 
    onLoginFail
    ) => {
    return (dispatch) => {
        userServ.postLogin(dataLogin)
            .then((res) => {
                // console.log(res);
                // dispatch({
                //     type: SET_USER,
                //     payload: res.data.content,
                // })
                dispatch(setUserLoginSuccess())
            })
            .catch((err) => {
                console.log(err);
            });
    }
}