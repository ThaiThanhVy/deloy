import axios from "axios"
import { BASE_URL, https, TOKEN_CYBERSOFT } from "./ConFigURL"

export const movivesServ = {
    // C1
    // getListMovie: () => {
    //     return axios({
    //         url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03`,
    //         method: "GET",
    //         // Bổ sung thông tin cho backEnd biết
    //         headers: {
    //             TokenCybersoft: TOKEN_CYBERSOFT,
    //         }
    //     });
    // },

    //C2
    getListMovie: () => {
        let url = "/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03"
        return https.get(url)
    },

    // getMovieByTheater: () => {
    //     return axios({
    //         url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03`,
    //         method: "GET",
    //         headers: {
    //             TokenCybersoft: TOKEN_CYBERSOFT,
    //         }
    //     })
    // }

    getMovieByTheater: () => {
        let url = "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03"
        return https.get(url)
    }
};