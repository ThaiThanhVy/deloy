const USER = 'USER'

export const localServ = {
    user: {

        // Data đang là object nên muốn lưu nó dưới dạng Json 
        // Ta phải chuyển nó về dạng Json = stringify
        // setItem là lưu nó lại
        set: (dataLogin) => {
            let jsonData = JSON.stringify(dataLogin)
            localStorage.setItem(USER, jsonData)
        },

        // getItem là lấy cái JSON đó
        get: () => {
            let jsonData = localStorage.getItem(USER);
            // trả data nó lại thành {} bằng cách parse trả về cho backend
            if (jsonData) {
                return JSON.parse(jsonData)
            } else {
                return null
            }
        },
        remove: () => {
            localStorage.removeItem(USER);
        }
    }
}