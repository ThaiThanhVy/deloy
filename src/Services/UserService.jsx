import axios from "axios"
import { BASE_URL, https, TOKEN_CYBERSOFT } from "./ConFigURL"

// export const userServ = {
//     postLogin: (data) => {
//         let url = "/api/QuanLyNguoiDung/DangNhap"

//         // data
//         console.log('data: ', data);
//         // Bổ sung thông tin cho backEnd biết
//         return https.post(data)
//     },
// };

export const userServ = {
    postLogin: (dataLogin) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
            method: "POST",
            data: dataLogin,
            // Bổ sung thông tin cho backEnd biết
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
            },
        });
    },
};


// getListMovie: () => {
//     let url = "/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03"
//     return https.get(url)
// },