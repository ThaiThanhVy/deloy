import React from 'react'
import UserNav from '../UserNav'

export default function Header() {
    return (
        <div className='shadow container'>
            <div className='flex justify-between items-center mx-auto h-20'>
                <span className='text-yellow-500 text-2xl font-medium animate-bounce'>CyberMovie</span>
                <UserNav />
            </div>
        </div>

    )
}
