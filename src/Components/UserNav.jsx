import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { localServ } from '../Services/LocalService'

export default function UserNav() {
    let user = useSelector((state) => {
        return state.userReducer.userInfor
    })
    console.log('user: ', user);



    let handleLogout = () => {

        //Xóa dữ liệu từ localStorage
        localServ.user.remove();

        //remove data từ redux
        // dispatch({
        //     type: SET_USER
        //     payload: null
        // })

        window.location.href = "/"
    }


    let renderContent = () => {
        if (user) {
            return (
                <>
                    <span className='font-medium text-blue-500 underline'>{user.hoTen}</span>
                    <button onClick={() => { handleLogout() }} className='border rounded border-black px-5 py-2 bg-red-500 hover:bg-green-300 hover:text-white  transition duration-150'>Đăng Xuat</button>
                </>
            )
        } else {
            return (
                <>
                    <NavLink to="/login">
                        <button className='border rounded border-black px-5 py-2 hover:bg-green-300 hover:text-white  transition duration-150'>Đăng Nhập</button>
                    </NavLink>
                    <button className='border rounded border-red-500 px-5 py-2 text-red-500 hover:bg-blue-300'>Đăng Kí</button>

                </>
            )
        }
    }
    return (
        <div className='space-x-5'>
            {/* <NavLink to="/login">
                <button className='border rounded border-black px-5 py-2 hover:bg-green-300 hover:text-white  transition duration-150'>Đăng Nhập</button>
            </NavLink>
            <button className='border rounded border-red-500 px-5 py-2 text-red-500 hover:bg-blue-300'>Đăng Kí</button> */}
            {renderContent()}
        </div>
    )
}
