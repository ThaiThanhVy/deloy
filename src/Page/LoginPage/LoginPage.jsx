import { Button, Checkbox, Form, Input, message } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { SET_USER } from "../../Redux/Constants/ConstantUser";
import { localServ } from "../../Services/LocalService";
import { userServ } from "../../Services/UserService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/bg_login.json";
const LoginPage = () => {
  let navigate = useNavigate();

  let dispatch = useDispatch();

  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        // lưu vào localStorage
        localServ.user.set(res.data.content);

        // dispatch to store
        dispatch({
          type: SET_USER,
          payload: res.data.content,
        });

        // chuyển hướng sang trang chủ
        message.success("Dang Nhap Thanh Cong");
        setTimeout(() => {
          navigate("/");
        }, 2000);

        console.log(res);
      })
      .catch((err) => {
        message.success("Dang Nhap That Bai");
        console.log(err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    // screen = 100 vh \\ vw
    <div className="container mx-auto h-screen w-screen flex items-center justify-center">
      <div className="w-1/2 h-full">
        <Lottie animationData={bg_animate} loop={true} />
      </div>
      <div className="w-1/2 h-full flex items-center justify-center">
        <Form
          className="bg-blue-500 w-full"
          layout="vertical"
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 24,
          }}
          initialValues={{}}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          ></Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default LoginPage;
