import React from 'react'
import { Tabs } from 'antd';
import { useEffect } from 'react';
import { movivesServ } from '../../Services/MovieService';
import { useState } from 'react';
import ItemTabsMovie from './ItemTabsMovie';

export default function TabsMovies() {

    const [dataMovie, setDataMovie] = useState([])
    useEffect(() => {
        movivesServ.getMovieByTheater()
            .then((res) => {
                console.log(res);
                setDataMovie(res.data.content)
            })
            .catch((err) => {
                console.log(err);
            });
    }, [])

    const renderContent = () => {
        return dataMovie.map((heThongRap, index) => {
            return <Tabs.TabPane tab={<img className='w-16 h-16' src={heThongRap.logo}></img>} key={index}>

                {/* listCumRap trong API */}
                <Tabs style={{ height: 500 }} tabPosition="left">
                    {heThongRap.lstCumRap.map((cumRap, index) => {
                        return (
                            <Tabs.TabPane tab={
                                <div className='w-48 text-left'>
                                    <p className='text-gray-700 truncate'>
                                        {cumRap.tenCumRap}
                                    </p>
                                    <p className='truncate'>
                                        {cumRap.diaChi}
                                    </p>
                                </div>
                            }
                                key={index}
                            >
                                {
                                    <div
                                        // scrollbar react
                                        className='h-32 scrollbar scrollbar-thumb-blue-700 scrollbar-track-blue-300 overflow-y-scroll hover:scrollbar-thumb-green-700'
                                        style={{ height: 500, overflowY: "scroll" }}>
                                        {cumRap.danhSachPhim.map((phim, index) => {
                                            return <ItemTabsMovie key={index} data={phim} />
                                        })}
                                    </div>

                                }
                            </Tabs.TabPane>
                        )
                    })}
                </Tabs>

            </Tabs.TabPane>
        })
    }


    return (
        <div className='flex justify-center'>
            <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
                {renderContent()}
            </Tabs>
        </div>
    )
}
