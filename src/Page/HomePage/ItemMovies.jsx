import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function ItemMovies(props) {

    let { data } = props
    return (
        <Card
            className="container mt-3"
            hoverable
            style={{
                width: "100%",
            }}
            cover={<img className="h-80 w-full object-cover" alt="example" src={data.hinhAnh} />}
        >
            {/* truncate để tạo dấu ... */}
            <Meta title={<p className="text-red-500 truncate">{data.tenPhim}</p>} description="www.instagram.com" />
            <br />
            <NavLink to={`/detail/${data.maPhim}`}>
                <button className="w-full py-2 bg-red-500 text-center text-white rounded cursor-pointer transitionduration-300 hover:bg-zinc-500">Xem Chi Tiết</button>
            </NavLink>
        </Card>
    );
}

