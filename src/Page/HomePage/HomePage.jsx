import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { setLoadingOffAction, setLoadingOnAction } from '../../Redux/actions/ActionSpinner'
import { SET_LOADING_OFF, SET_LOADING_ON } from '../../Redux/Constants/ConstantSpinner'
// import Spinner from '../../Components/spinners/Spinner'
import { movivesServ } from '../../Services/MovieService'
import ItemMovies from './ItemMovies'
import TabsMovies from './TabsMovies'

export default function HomePage() {

  const [movies, setMovies] = useState([])
  // const [isLoading, setIsLoading] = useState(true)

  let dispatch = useDispatch();

  useEffect(() => {
    // setIsLoading(true)
    // Sử dụng ascyn wait
    // spinner
    dispatch(setLoadingOnAction())
    movivesServ.getListMovie()
      .then((res) => {
        // spinner
        dispatch(setLoadingOffAction())
        console.log(res)
        // setIsLoading(false)
        setMovies(res.data.content);
      })
      .catch((err) => {
        dispatch(setLoadingOffAction())
        // setIsLoading(false)
        console.log(err)
      })
  }, [])

  const renderMovies = () => {
    return movies.map((data, index) => {
      return <ItemMovies key={index} data={data} />
    })
  }


  return (
    <div className='container mx-auto space-y-10'>
      <div className='grid grid-cols-5 gap-10 mb-5 container'>
        {renderMovies()}
      </div>
      <br />
      <br />
      <TabsMovies />
      {/* {isLoading && <Spinner />} */}
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  )
}
